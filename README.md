**What's in the project:**
 
  1. New user registration
 2. Log in to the app
 3. Home page. It's a page with posts of all users. Here you could to create and share your post
 4. Friends page. Here you can see your friends in the "Friends" section and all other users in the "Find friends" section
    Each user card in "Find friends" has a link to send him a friend request. If you already sent the friend request,
    you will see that instead of friend request link
 5. Navigation bar. It has two nav items "Home" and "Friends" for navigation between pages. On the right corner of the bar
    there are log out button and notification icon. If you have any notifications, their count will be added to the notification item 
    and the notifications list will be open when you click on the item. Here you could to accept or deny friend requests of other users
    
    Enjoy :-)
    
**Issues:**

1. Navigation tabs are not selected

**TODO:**

1. Solve the issue of navigation tabs
2. Create separate files for all functions on the client side
3. Add constants
 

**How to run this project locally on your computer**

*Requires :NodeJS*

1. Download .zip file of this project
2. Extract project files to the folder you wish

/* Run local server */
1. Open Terminal (MAC) or cmd (Windows) 
2. Navigate to the backend directory of the project:
    
    cd <path to project folder>/social-app-master/backend
    
3. Type npm install 

    This command will install dependencies of the server side of this project

4. Type npm start

    This command will run the local server
    You shoul see the following:
    
    > backend@0.0.0 start 'path to project folder'/social-app-master/backend
    
    > node ./bin/www

    >connected to the database 

/* Run client side */    
1. Open a new window of your Terminal/ cmd
2. Navigate to the backend directory of the project:
    
    cd <path to project folder>/social-app-master/backend
3. Type npm install

    This command will install dependencies of the client side of this project
    
4. Type npm start

    This command will open this app on your default Browser.
    
    *Note that this project was tested on Chrome only. If your default browser is not Chrome,
     please open Chrome and type on the browser line http://localhost:3000
