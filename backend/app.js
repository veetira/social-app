const createError = require("http-errors");
const express = require("express");
const path = require("path");
const cookieParser = require("cookie-parser");
const logger = require("morgan");
const cors = require("cors");
const bodyParser = require('body-parser');
const session = require("express-session");
// const FileStore = require('session-file-store')(session);
const passport = require("passport");
const flash    = require('connect-flash');
const mongoose = require("mongoose");

const indexRouter = require("./routes/index");
const usersRouter = require("./routes/users");
const postsRouter = require("./routes/posts");

const app = express();
const localStorage = require("node-persist");
const configDB = require('./config/mongoDB');

localStorage.init();
// const dbRoute = "mongodb+srv://@cluster0-pxnal.mongodb.net/social_app?retryWrites=true";
mongoose.connect(configDB.url, {useNewUrlParser: true});

let db = mongoose.connection;

db.once("open", () => console.log("connected to the database " ));

// checks if connection with the database is successful
db.on("error", console.error.bind(console, "MongoDB connection error:"));

let allowCrossDomain = function(req, res, next) {
    res.header('Access-Control-Allow-Origin', "*");
    res.header('Access-Control-Allow-Headers', "*");
    next();
};
const isLoggedIn = async (req, res, next) => {
    const userEmail = await localStorage.getItem("user_email"),
        requestPath = req.originalUrl;
    // if user is authenticated in the session, carry on
    if ((userEmail && userEmail.length) || requestPath === "/users/login" || requestPath === "/users/signup") {
        next();
    }
    else {
        res.status(401).send({
            success: false,
            error: "User not logged in"
        });
    }

};
app.use(logger('dev')); // log every request to the console
app.use(cookieParser("my_secret")); // read cookies (needed for auth)
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "jade");

app.use(session({
    secret: "my_secret",
    resave: true,
    saveUninitialized: true,
    cookie: {
        maxAge: 24 * 60 * 60 * 1000
    }
}));
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());
app.use(cors());
//app.use(logger("dev"));
app.use(express.static(path.join(__dirname, "public")));
app.use(isLoggedIn)
app.use(allowCrossDomain);

app.use("/", indexRouter);
app.use("/users", usersRouter);
app.use("/posts", postsRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get("env") === "development" ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render("error");
});

module.exports = app;
