const express = require('express');
const router = express.Router();
const passport = require("passport");
const localStorage = require("node-persist");
const userModel = require("../models/user");

require("async");
require("await");
require('../config/passport')(passport);

localStorage.init();

/**
 * Check if the user is authenticated
 * @param req
 * @param res
 * @param next
 * @returns {Promise<*>}
 */
const isLoggedIn = async (req, res, next) => {
    const userEmail = await localStorage.getItem("user_email");
    /* If the user is authenticated, carry on*/
    if (userEmail && userEmail.length) {
        return next();
    }
    res.status(401).send({
        success: false,
        error: "User not logged in"
    });
};


/**
 * Get logged in user email from Local Storage
 * @returns {Promise<void>}
 */
const getUserEmail = async () => {
    return await localStorage.getItem("user_email");
};

/**
 * Registration
 */
router.post("/signup", (req, res, next) => {
        passport.authenticate("signup",function(err, user, info) {
            if (err) {
                console.log("Error registration: ", err);
                return next(err);
            }
            if (!user) {
                console.log("Error registration: ", info.message);
                return res.status(401).send({
                    success: false,
                    error: info.message
                });
            }
            res.send({
                success: true
            });

        })(req, res, next);

    });

/**
 * Login to app
 */
router.post("/login", async (req, res, next) => {
    await localStorage.clear();
    passport.authenticate("login", function(err, user, info) {
        if (err) {
            console.log("Error login: ", err);
            return next(err);
        }
        if (!user) {
            console.log("Error login: ", info.message);
            return res.status(401).send({
                success: false,
                error: info.message
            });
        }
        else {
            req.logIn(user, function(err) {
                if (err) {
                    console.log("Error login: ", err);
                    return res.status(401).send({
                        success: false,
                        error: info.message
                    });
                }
                req.session.user = user;
                localStorage.setItem("user_email", user.email);
                res.send({
                    success: true
                });
            });
        }

    })(req, res, next);
});

router.get('/logout', async function(req, res) {
    await localStorage.clear();
    req.logout();
    res.send({
        success: true
    });
});

/**
 * Get info of the logged in user
 */
router.get("/get_user_info", isLoggedIn, async (req, res, next) => {
    const userEmail = await getUserEmail();
    if (userEmail) {
        try {
            userModel.findOne({email: userEmail}, "name src", (err, data) => {
                if (err) {
                    res.status(401).send({
                        success: false,
                        error: err
                    });
                }
                else {
                    res.send({
                        success: true,
                        data: data
                    });
                }
            });
        }
        catch (err) {
            res.status(401).send({
                success: false,
                error: err
            });
        }
    }
    else {
        res.status(401).send({
            success: false,
            error: "User not logged in"
        });
    }
});

/**
 * Get list of all users
 */
router.get("/", (req, res, next) => {

});

/**
 * Get friends of the logged in user
 */
router.get('/friends', isLoggedIn, async (req, res, next) => {
    const userEmail = await getUserEmail();
    if (userEmail) {
        userModel.find({friends:userEmail},"src name city",(err, data) => {
            if (err) {
                res.status(401).send({
                    success: false,
                    error: err
                });
            }
            else {
                res.send({
                    success: true,
                    data: data
                });
            }

        });
    }
    else {
        console.log("User not logged in");
        res.status(401).send({
            success: false,
            error: "User not logged in"
        });
    }
});

/**
 * This function returns friend request list of the logged in user by his email
 * @param userEmail
 * @returns {Promise<*>}
 */
const getUserFriendRequestList = async (userEmail) => {
    const data = await userModel.findOne({email: userEmail}, "friend_requests")
    return data.friend_requests || [];
};

/**
 * Get users that are not friends of the logged in user
 */
router.get('/not_friends', isLoggedIn, async (req, res, next) => {
    const reqQuery = req.query,
        userEmail = await getUserEmail(),
        userFriendRequests = await getUserFriendRequestList(userEmail),
        friendRequestsUsersQuery = {email: {$in:userFriendRequests}},
        otherNotFriendsQuery = {$and:[
                {friends: {$ne:userEmail}},
                {email:{$ne:userEmail}},
                {email:{$nin: userFriendRequests}}
            ]},
        fieldsToRetrieve = "src name city email";
    let friendRequestsUsers,
        otherNotFriends;
    userModel.find(friendRequestsUsersQuery, fieldsToRetrieve, (err, data) => {
        if (!err) {
            friendRequestsUsers = data;
            userModel.find(otherNotFriendsQuery, fieldsToRetrieve, (err, data) => {
                if (!err) {
                    otherNotFriends = data;
                    res.send({
                        success: true,
                        friendRequestsUsers: friendRequestsUsers,
                        otherNotFriends: otherNotFriends
                    });
                }
                else {
                    console.log("Error in retrieving not friends data: " + err);
                    res.status(400).send({ success: false, error: err });
                }
            });
        }
        else {
            console.log("Error in retrieving not friends data: " + err);
            res.status(400).send({ success: false, error: err });
        }
    });
});

/**
 * Send friend request
 */
router.post("/send_request", isLoggedIn, async (req, res, next) => {
    try {
        const params = req.body,
            userEmail = await getUserEmail(),
            requestTargetEmail = params.requestEmail;
        userModel.updateOne({email: userEmail}, {$addToSet:{friend_requests: requestTargetEmail}}, (err, response) => {
            if (err) {
                console.log("Error in adding friend request: " + err);
                res.status(400).send({ success: false, error: err });
            }
            else if (response.n === 0 || response.nModified === 0) {
                console.log("Error in adding friend request");
                res.status(400).send({
                    success: false,
                    error: "The record didn't updated on DB"
                });
            }
            else {
                res.status(200).send({
                    success: true,
                    message: response
                });
            }
        });
    }
    catch (err) {
        console.log("Error in adding friend request: " + err);
    }
});

/**
 * Get friend requests that were sent to logged in user
 */
router.get("/get_friend_requests", isLoggedIn, async(req, res, next) => {
    const userEmail = await getUserEmail();
    userModel.find({friend_requests: userEmail}, "name email src", (err, data) => {
        if (err) {
            console.log("Error in retrieving friend requests: " + err);
            res.status(400).send({ success: false, error: err });
        }
        else {
            res.status(200).send({
                success: true,
                data: data
            });
        }
    })
});

/**
 * This function receives an email of the user who sent request, finds his data in "users" collection
 * and removes the request from his friend request list by email of the request target user
 * @param requestSenderEmail
 * @param requestTargetEmail
 * @returns {Promise<*>}
 */
const removeFriendReqFromTheList = async(requestSenderEmail, requestTargetEmail) => {
    return await userModel.updateOne({email: requestSenderEmail}, {$pullAll: {friend_requests: [requestTargetEmail]}});
};

/**
 * This function adds a new friend's email to the friend list of the logged in user
 * @param userEmail
 * @param newFriendEmail
 * @returns {Promise<*>}
 */
const addFriendToFriendList = async(userEmail, newFriendEmail) => {
    return await userModel.updateOne({email: userEmail}, {$addToSet:{friends: newFriendEmail}});
};

/**
 * Accept friend request:
 *  1. Remove friend request from sender's friend requests
 *  2. Remove friend request from logged in user, if he also sent request to the sender
 *  3. Add a sender email to friend list of the logged in user
 *  4. Add an email of logged in user to friend list of the request sender
 *  @requires email of request sender
 */
router.post("/accept_friend_request", isLoggedIn, async(req, res, next) => {
    try {
        const userEmail = await getUserEmail(),
            requestSenderEmail = req.body.requestSenderEmail;
        /* Remove friend request from sender's friend requests */
        const removeReqFromSender = await removeFriendReqFromTheList(requestSenderEmail, userEmail);
        if (!removeReqFromSender.ok) {
            const error = removeReqFromSender.error;
            console.log("Error in removing friend request: " + error);
            res.status(400).send({ success: false, error: error });
        }
        else {
            /* Remove friend request from logged in user, if he also sent request to the sender */
            const removeReqFromUser = await removeFriendReqFromTheList(userEmail, requestSenderEmail);
            if (!removeReqFromUser.ok) {
                const error = removeReqFromSender.error;
                console.log("Error in removing friend request: " + error);
                res.status(400).send({ success: false, error: error });
            }
            else {
                /* Add a sender email to friend list of the logged in user */
                const addSenderToFriends = await addFriendToFriendList(userEmail, requestSenderEmail);
                if (!addSenderToFriends.ok) {
                    const error = addSenderToFriends.error;
                    console.log("Error in adding new friend: " + error);
                    res.status(400).send({ success: false, error: error });
                }
                else {
                    /* Add an email of logged in user to friend list of the request sender */
                    const addUserToSenderFriends = await addFriendToFriendList(requestSenderEmail, userEmail);
                    if (!addUserToSenderFriends.ok) {
                        const error = addUserToSenderFriends.error;
                        console.log("Error in adding new friend: " + error);
                        res.status(400).send({ success: false, error: error });
                    }
                    else {
                        res.status(200).send({
                            success: true,
                            message: addUserToSenderFriends
                        });
                    }
                }
            }
        }
    }
    catch (err) {
        console.log("Error in friend request accepting: " + err);
        res.status(400).send({ success: false, error: err });
    }
});

/**
 * Remove friend request from sender's friend requests
 * @requires email of request sender
 */
router.post("/deny_friend_request", isLoggedIn, async(req, res, next) => {
    try {
        const userEmail = await getUserEmail(),
            requestSenderEmail = req.body.requestSenderEmail;
        /* Remove friend request from sender's friend requests */
        const removeReqFromSender = await removeFriendReqFromTheList(requestSenderEmail, userEmail);
        if (!removeReqFromSender.ok) {
            const error = removeReqFromSender.error;
            console.log("Error in removing friend request: " + error);
            res.status(400).send({ success: false, error: error });
        }
        else {
            res.status(200).send({
                success: true,
                message: removeReqFromSender
            });
        }
    }
    catch (err) {
        console.log("Error in friend request rejecting: " + err);
        res.status(400).send({ success: false, error: err });
    }
});
module.exports = router;
