const express = require('express');
const router = express.Router();
const passport = require("passport");
const LocalStrategy = require('passport-local');
const localStorage = require("node-persist");
const userModel = require("../models/user");
const postModel = require("../models/post");
const moment = require("moment");
require("async");
require("await");
localStorage.init();

/**
 * Get email of the logged in user from Local Storage
 * @returns {Promise<null>}
 */
const getUserEmail = async () =>{
    const userEmail = await localStorage.getItem("user_email");
    return userEmail && userEmail.length ? userEmail : null;
};

/**
 * Get all posts
 */
router.get("/", (req, res, next) => {
    try {
        postModel.aggregate([
            {
                $lookup:{
                    from: "users",
                    localField: "author",
                    foreignField: "email",
                    as: "post_author"
                }
            },
            {
                $replaceRoot: { newRoot: { $mergeObjects: [ { $arrayElemAt: [ "$post_author", 0 ] }, "$$ROOT" ] } }
            },
            { $project: { post_author: 0, friends: 0, friend_requests: 0 , password: 0, city: 0, email: 0, author:0} },

            { $sort: { "date": -1 } }
        ], async (err, data) => {
            if (err) {
                console.log("ERROR: ", err);
                res.send({
                    success: false,
                    error: err
                });
            }
            else {
                const userEmail = await localStorage.getItem("user_email");
                const userAvatarSrc = await localStorage.getItem("user_src");
                res.send({
                    success: true,
                    data: data,
                    userEmail: userEmail,
                    userAvatarSrc: userAvatarSrc
                });
            }
        });
    }
    catch (err) {
        console.log("ERROR: ", err);
    }
});

/**
 * Save a new post
 */
router.post("/create_post", async (req, res, next) => {
    try {
        const postData = req.body,
            userEmail = await getUserEmail();
        if (userEmail) {
            postData.date = moment().format('YYYY-MM-DD, h:mm a');
            postData.author = userEmail;
            postModel.create(postData, (err, data) => {
                if (err) {
                    console.log("Post creation failed: ", err);
                    res.status(400).send({
                        success: false,
                        error: err
                    });
                }
                else {
                    console.log("Post creation succeeds: ", JSON.stringify(data));
                    res.send({
                        success: true,
                        data: data
                    });
                }
            });
        }
    }
    catch (err) {
        console.log("Post creation failed: ", err);
        res.status(400).send({
            success: false,
            error: err
        });
    }
});

module.exports = router;
