const mongoose = require('mongoose');
const crypto = require('crypto');
const jwt = require('jsonwebtoken');

const { Schema } = mongoose;

const authSchema = new Schema({
    email: String,
    hash: String
});

authSchema.methods.setPassword = function(password) {
    this.hash = btoa(password)
};

authSchema.methods.validatePassword = function(password) {
    return this.hash === btoa(password);
};

authSchema.methods.generateJWT = function() {
    const today = new Date();
    const expirationDate = new Date(today);
    expirationDate.setDate(today.getDate() + 60);

    return jwt.sign({
        email: this.email,
        id: this._id,
        exp: parseInt(expirationDate.getTime() / 1000, 10),
    }, 'secret');
}

authSchema.methods.toAuthJSON = function() {
    return {
        _id: this._id,
        email: this.email,
        token: this.generateJWT(),
    };
};
module.exports = mongoose.model('Auth', authSchema);