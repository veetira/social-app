const mongoose = require('mongoose');

const Schema = mongoose.Schema;
const postSchema = new Schema({
    author: String,
    message: String,
    date: String
});

module.exports = mongoose.model("Post", postSchema);