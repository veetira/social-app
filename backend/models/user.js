const mongoose = require('mongoose');
const btoa = require("btoa");

const Schema = mongoose.Schema;
const userSchema = new Schema({
    email: String,
    password: String,
    src: String,
    name: String,
    city: String,
    friends: Array,
    friend_requests:Array
});

userSchema.methods.encodePassword = function(password) {
    return this.password = btoa(password);
};

userSchema.methods.validatePassword = function(password) {
    return this.password === btoa(password);
};

module.exports = mongoose.model("User", userSchema);
