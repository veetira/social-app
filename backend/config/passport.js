const LocalStrategy = require('passport-local');
const User = require("../models/user");

module.exports = function(passport) {
    passport.use("login", new LocalStrategy({
            usernameField: 'email',
            passwordField: 'password'
        },
        function(username, password, done) {
            User.findOne({ email: username }, function(err, user) {
                if (err) { return done(err); }
                if (!user) {
                    console.log('Incorrect username.');
                    return done(null, false, { message: 'Incorrect username.' });
                }
                if (!user.validatePassword(password)) {
                    console.log('Incorrect password.');
                    return done(null, false, { message: 'Incorrect password.' });
                }
                return done(null, user);
            });
        }
    ));

    passport.use("signup", new LocalStrategy({
            usernameField : 'email',
            passwordField : 'password',
            passReqToCallback : true
        },
        function(req, email, password, done) {
            /* Check if the user trying to login already exists */
            User.findOne({ 'email' :  email }, function(err, user) {
                if (err)
                    return done(err);

                /* Check if there is already a user with that email */
                if (user) {
                    return done(null, false, { message: 'That email is already taken' });
                } else {

                    /* If there is no user with that email create new one */
                    let newUser = new User({
                        email: email,
                        name: req.body.name,
                        city: req.body.city
                    });
                    newUser.password = newUser.encodePassword(password);
                    /* Save the user*/
                    newUser.save(function(err) {
                        if (err)
                            throw err;
                        return done(null, newUser);
                    });
                }

            })}));

    passport.serializeUser(function(user, done) {
        done(null, user.email);
    });

    passport.deserializeUser(function(id, done) {
        User.findById(id, function(err, user) {
            done(err, user);
        });
    });

}