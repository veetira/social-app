import React from "react";
import { Route, Switch } from "react-router-dom";
import Registration from "./containers/auth/Registration";
import Login from "./containers/auth/Login";
import NotFound from "./containers/notFound/NotFound";
import Home from "./containers/home/Home";
import Friends from "./containers/people/People";
import Profile from "./containers/profile/Profile";

export default () =>
    <Switch>
        <Route path="/" exact component={Login} />
        <Route path="/home" exact component={Home} />
        <Route path="/login" exact component={Login} />
        <Route path="/registration" exact component={Registration} />
        <Route path="/friends" exact component={Friends} />
        <Route path="/profile" exact component={Profile}/>
        /*  Catch all unmatched routes */
        <Route component={NotFound} />
    </Switch>;