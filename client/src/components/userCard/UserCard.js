import React from "react";
import {MDBCol } from "mdbreact";
import serverConfig from "../../config/server";
import {getUser} from "../mainNavBar/MainNavBar";

const baseUrl = serverConfig.protocol + "://" + serverConfig.host + ":" + serverConfig.port + "/users";

export default class UserCard extends React.Component{
    constructor(props){
        super(props);
    }
    updateFriendRequest = (requestEmail) => {
        return fetch(baseUrl + "/send_request", {
            method: "POST",
            body: JSON.stringify({requestEmail: requestEmail}),
            headers:{
                'Content-Type': 'application/json'
            }
        }).then(
            response => response.json()
        ).catch(
            error => console.error('Error:', error)
        );
    };

    updateRequestLink = (selectedLink) => {
        const newEl = document.createElement("p");
        newEl.innerText = "Friend request sent";
        newEl.setAttribute("class", "font-weight-bold ml-1");
        newEl.setAttribute("style", "color: blue");
        selectedLink.replaceWith(newEl);
    };
    requestSent = async (requestEmail, ev) => {
            const selectedLink = ev.target,
               sentReqResponse = await this.updateFriendRequest(requestEmail);
            if (!sentReqResponse.success) {
                const error = sentReqResponse.error || "Response sending failed";
                alert(error);
                return;
            }
            this.updateRequestLink(selectedLink);
    };
    addFriendRequest = (requestEmail) => {
        if (this.props.friendRequest !== "true") {
            return "";
        }

        return this.props.requestSent ?
            <p className="font-weight-bold ml-1" style={{color: 'blue'}}>
                Friend request sent
            </p>:
            <a className="dark-grey-text font-weight-bold ml-1" onClick={this.requestSent.bind(this, requestEmail)}>
                Send friend request
            </a>;
    };

    render () {
        return (
            <MDBCol lg="3" md="6" className="mb-lg-0 mb-5" style={{paddingTop: 20}}>
                <img
                    src={this.props.src}
                    className="rounded-circle z-depth-1 img-fluid"
                    alt="Sample avatar"
                    style={{maxWidth: 60 + "%"}}
                />
                <h5 className="font-weight-bold dark-grey-text my-4">
                    {this.props.name}
                </h5>
                <h6 className="text-uppercase grey-text mb-3">{this.props.city}</h6>
                {this.addFriendRequest(this.props.email)}

            </MDBCol>
        )
    }
}