import React from "react";
import {MDBPopoverBody} from "mdbreact";
import Avatar from "../avatar/Avatar";

export default class SingleNotification extends React.Component {
    constructor (props) {
        super(props);
    }
    render () {
        return (
            <MDBPopoverBody style={{borderBottom: "1px solid #ebebeb"}}>
                <div className="mdb-feed">
                    <div className="news">
                        <div className="post-header" style={{padding: 0}}>
                            <Avatar src={this.props.src} style={{marginRight: 10}}/>
                            <div className="post-title">
                                <strong className="primary-font" style={{display: "block"}}>{this.props.name}</strong>
                            </div>
                        </div>
                        <div className="excerpt">
                            <div className="brief">
                                Sent you friend request
                            </div>
                            <div className="feed-footer">
                                <a href="#!" data-email={this.props.email} className="like" onClick={this.props.onAccept}>
                                    <span data-email={this.props.email}>Accept</span>
                                </a>
                                <a href="#!" className="like" onClick={this.props.onDeny}>
                                    <span data-email={this.props.email} style={{float: "right"}}>Deny</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </MDBPopoverBody>
        )
    }
}