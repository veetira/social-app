import React from "react";
import { MDBNavbar, MDBBadge, MDBNavbarNav, MDBNavItem, MDBNavLink, MDBNavbarToggler, MDBCollapse, MDBDropdown,
    MDBDropdownToggle, MDBNavbarBrand, MDBIcon, MDBPopover, MDBPopoverHeader} from "mdbreact";
import SingleNotification from "../singleNotification/SingleNotification";
import {isLoggedIn} from "../../containers/auth/Login";
import {getCurrentFriends, getCurrentNotFriends, updateFriends, updateNotFriends} from "../../containers/people/People";
import serverConfig from "../../config/server";
import "./MainNavBar.css";

const baseUrl = serverConfig.protocol + "://" + serverConfig.host + ":" + serverConfig.port + "/users";

export function getUser () {
    return this.state.user;
}

export default class MainNavBar extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isOpen: false,
            user: {},
            notifications: []
        };
        getUser=getUser.bind(this);
    };

    toggleCollapse = () => {
        this.setState({ isOpen: !this.state.isOpen });
    };

    removeReqFromNotifications = (notifications, requestSenderEmail) => {
        for (let i = notifications.length - 1; i >= 0; i--) {
            if (notifications[i].email === requestSenderEmail) {
                notifications.splice(i, 1);
            }
        }
    };

    acceptedFriendData = (notFriends, requestSenderEmail) => {
        let newFriend;
        for (let i = notFriends.length - 1; i >= 0; i--) {
            if (notFriends[i].email === requestSenderEmail) {
                newFriend = notFriends[i];
                notFriends.splice(i, 1);
                break;
            }
        }
        return newFriend;
    };

    /**
     * Get data of the logged in user from DB
     */
    getUserInfo() {
        fetch(baseUrl + "/get_user_info")
            .then(res => {
                return res.json();
            })
            .then(dataJson => {
                if (dataJson.success) {
                    this.setState({
                        user: dataJson.data
                    });
                }
                else {
                    window.location.pathname = "login";
                    alert(dataJson.error);
                }
            })
            .catch(err => {
                alert(err)
            });
    }

    /**
     * Get friend requests that were sent to the logged in user
     * and add them to the notifications list
     */
    getNotifications(){
        fetch(baseUrl + "/get_friend_requests")
            .then(res => {
                return res.json();
            })
            .then(dataJson => {
                if (dataJson.success) {
                    this.setState({
                        notifications: dataJson.data
                    });
                }
                else {
                    window.location.pathname = "login";
                    console.log(dataJson.error)
                }
            })
            .catch(err => {
                console.log(err)
            });
    }

    /**
     * When the friend request is accepted, it should be removed from the notification list
     * and the accepted user should be moved from not friends to friends
     * @returns {function(*)}
     */
    handleRequestAccept () {
        const me = this;
        return (ev) => {
            ev.preventDefault();
            const target = ev.target,
                requestSenderEmail = target.getAttribute("data-email");
            /* Send the email of the accepted request to update friendship data in DB*/
            fetch(baseUrl + "/accept_friend_request", {
                    method: "POST",
                    body: JSON.stringify({requestSenderEmail: requestSenderEmail}),
                    headers:{
                        'Content-Type': 'application/json'
                    }
                })
                .then(res => {
                    return res.json();
                })
                .then(dataJson => {
                    const notifications = me.state.notifications,
                        friends = getCurrentFriends(),
                        notFriends = getCurrentNotFriends();
                    /* Remove accepted request from notification list and
                    * update notifications state*/
                    this.removeReqFromNotifications(notifications, requestSenderEmail);
                    me.setState({
                        notifications: notifications
                    });
                    /* Update state of friends and not friends lists*/
                    friends.push(this.acceptedFriendData(notFriends, requestSenderEmail));
                    updateFriends(friends);
                    updateNotFriends(notFriends);
                })
                .catch(err => {
                    alert(err)
                });
        }
    }

    /**
     * When the friend request is denied, it should be removed from the notification list
     * @returns {function(*)}
     */
    handleRequestDeny () {
        const me = this;
        return (ev) => {
            ev.preventDefault();
            const target = ev.target,
                requestSenderEmail = target.getAttribute("data-email");
            fetch(baseUrl + "/deny_friend_request", {
                method: "POST",
                body: JSON.stringify({requestSenderEmail: requestSenderEmail}),
                headers:{
                    'Content-Type': 'application/json'
                }
            })
                .then(res => {
                    return res.json();
                })
                .then(dataJson => {
                    const notifications = me.state.notifications;
                    /* Remove accepted request from notification list and
                    * update notifications state*/
                    this.removeReqFromNotifications(notifications, requestSenderEmail);
                    me.setState({
                        notifications: notifications
                    });
                })
                .catch(err => {
                    alert(err)
                });
        }
    }

    /**
     * Send a logout request to reset user data kept to authentication on the server
     * and reset his data kept in the component state
     */
    logOut = () => {
        fetch(baseUrl + "/logout") .then(res => {
            return res.json();
        })
            .then(dataJson => {
                if (dataJson.success) {
                    this.setState({
                        user: {}
                    });
                    window.location.pathname = "login";
                }
                else {
                    alert("Logout failed")
                }
            })
            .catch(err => {
                alert(err)
            });
    };

    componentDidMount() {
        this.getUserInfo();
        this.getNotifications();
    }

    render() {
        const NotificationList = () => {
                const notificationsData = this.state.notifications;
                /*Build notifications by received data*/
                return  notificationsData.map((notification) => {
                    return (
                        <SingleNotification
                            key={notification.email}
                            email={notification.email}
                            src={notification.src}
                            name={notification.name}
                            onAccept={this.handleRequestAccept()}
                            onDeny={this.handleRequestDeny()}
                        />)
                })
            },
            NotificationsPanel = () => {
                const notificationData = this.state.notifications;
                return notificationData && notificationData.length ?
                    <div style={{width: 250}}>
                        <MDBPopoverHeader>You have new friend requests:</MDBPopoverHeader>
                        <NotificationList/>
                    </div>
                    :
                    "";
            },
            NotificationCount = () => {
                const notificationData = this.state.notifications,
                    count = notificationData.length;
                return count ?
                    <MDBBadge color="danger" style={{marginLeft: 0 + "!important", fontSize: 60 + "%", position: "absolute"}}>{count}</MDBBadge>
                    : "";
            };
        return (
            isLoggedIn ?
            <MDBNavbar color="default-color" dark expand="md" fixed="top">
                <MDBNavbarToggler onClick={this.toggleCollapse} />
                <MDBCollapse id="navbarCollapse3" isOpen={this.state.isOpen} navbar>
                    <MDBNavbarNav left>
                        <MDBNavItem>
                            <MDBNavbarBrand href="#">Hello, {this.state.user.name}</MDBNavbarBrand>
                        </MDBNavItem>
                        <MDBNavItem>
                            <MDBNavLink to="/home" className="nav-bar-link">Home</MDBNavLink>
                        </MDBNavItem>
                        <MDBNavItem>
                            <MDBNavLink to="/friends" className="nav-bar-link">Friends</MDBNavLink>
                        </MDBNavItem>
                    </MDBNavbarNav>
                    <MDBNavbarNav right>
                        <MDBNavItem style={{marginRight:20}}>
                            <MDBDropdown>
                                <MDBPopover placement="bottom" popover clickable id="popper3">
                                    <MDBDropdownToggle nav>
                                        <MDBIcon icon="bell" size="lg"/>
                                        <NotificationCount/>
                                    </MDBDropdownToggle>
                                    <NotificationsPanel/>
                                </MDBPopover>
                            </MDBDropdown>
                        </MDBNavItem>
                        <MDBNavItem>
                            <MDBDropdown>
                                <MDBDropdownToggle nav>
                                    <MDBIcon icon="sign-out-alt" size="lg" onClick={this.logOut}/>
                                </MDBDropdownToggle>
                            </MDBDropdown>
                        </MDBNavItem>
                    </MDBNavbarNav>
                </MDBCollapse>
            </MDBNavbar> : ""
        );
    }
}