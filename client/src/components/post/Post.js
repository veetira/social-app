import React, {Component} from "react";
import { MDBCard, MDBCardBody} from "mdbreact";
import Avatar from "../avatar/Avatar"
import "./Post.css";

export default class Post extends Component {
    render () {
        return (
            <li className="chat-message d-flex justify-content-between mb-4">
                <MDBCard style={{maxWidth: 700, width: 60 + "%", minWidth: 400}}>
                    <MDBCardBody>
                        <div className="post-header">
                            <Avatar src={this.props.avatarSrc} style={{flex: "0 0 auto"}}/>
                            <div className="post-title">
                                <strong className="primary-font" style={{display: "block"}}>{this.props.auth}</strong>
                                <small className="pull-right text-muted">
                                    <i className="far fa-clock" /> {this.props.when}
                                </small>
                            </div>

                        </div>
                        <hr />
                        <p className="mb-0">{this.props.message}</p>
                    </MDBCardBody>
                </MDBCard>
            </li>
        )
    }
}