import React, {Component} from "react";
import { MDBCard, MDBCardBody, MDBIcon} from 'mdbreact';
import Avatar from "../avatar/Avatar";
import NewPostModal from "../newPostModal/NewPostModal";
import "./CreatePost.css";

export default class CreatePost extends Component {
    render () {
        return (
            <li className="chat-message d-flex justify-content-between mb-4">
                <MDBCard style={{minWidth: 400, backgroundColor: "lavender"}}>
                    <MDBCardBody style={{padding: 0}}>
                        <a className="create-post-btn" onClick={this.props.toggle}>
                            <Avatar src={this.props.userAvatar} style={{flex: "0 0 auto"}}/>
                            <span className="pull-right text-muted" style={{marginLeft: 15+"px"}}>
                                Create post
                            </span>
                        </a>
                    </MDBCardBody>
                </MDBCard>
                <NewPostModal
                    header="Create post" isOpen={this.props.isOpen} toggle={this.props.toggle} contentLabel="What's on your mind?"
                    rows="5" onCloseClick={this.props.toggle} onShareClick={this.props.onShareClick} shareDisabled={this.props.shareDisabled}
                    onContentChanged={this.props.onContentChanged}
                />
            </li>
        )
    }
}