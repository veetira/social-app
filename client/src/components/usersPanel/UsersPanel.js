import React, {Component} from "react";
import {MDBCardBody, MDBRow} from "mdbreact";
import UserCard from "../userCard/UserCard";

export default class UsersPanel extends Component {
    render () {
        const data = this.props.data;
        return ( data && data.length ?
            <MDBCardBody className="friends-panel" style={{height: 1000, overflow: "scroll"}}>
                <MDBRow>
                    {this.props.data.map((user, index) => {
                        let avatarSrc = user.src && user.src ? user.src : "https://cdn.pixabay.com/photo/2016/08/08/09/17/avatar-1577909_960_720.png";
                        /*Return user card*/
                        return (
                            <UserCard
                                key={index}
                                src={avatarSrc}
                                name={user.name}
                                city={user.city}
                                email={user.email}
                                requestSent = {user.requestSent}
                                friendRequest={this.props.friendRequest}
                            />)
                    })}
                </MDBRow>
            </MDBCardBody> : this.props.friendRequest !== "true" ? <h3 style={{color: "cadetblue", marginTop: 30}}>You have no friends yet</h3> :
            ""
        );
    };
}