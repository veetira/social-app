import React, {Component} from "react";
import {MDBIcon} from "mdbreact";
import "./Avatar.css";

export default class Avatar extends Component {
    render () {
        const avatarSrc = this.props.src,
            navBarAvatar = this.props.navBar,
            wrapperClass = navBarAvatar === "true" ? "nav-bar-avatar-wrapper" : "avatar-wrapper"
        return (
            avatarSrc && avatarSrc.length ? <div className={wrapperClass}>
                <img
                    src={this.props.src}
                    alt="avatar"
                    className="rounded-circle z-depth-1-half"
                />
            </div>:
                <MDBIcon icon="user" className="default-avatar"/>
        )
    }
}