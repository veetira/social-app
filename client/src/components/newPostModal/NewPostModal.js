import React, {Component} from "react";
import {MDBModal, MDBModalHeader, MDBModalBody, MDBModalFooter, MDBBtn} from "mdbreact";
import InputContentArea from "../inputContentArea/InputContentArea";

export default class NewPostModal extends Component {
    render () {
        return (
            <MDBModal isOpen={this.props.isOpen} toggle={this.props.toogle} centered>
                <MDBModalHeader toggle={this.props.toogle}>{this.props.header}</MDBModalHeader>
                <MDBModalBody>
                    <InputContentArea label={this.props.contentLabel} rows={this.props.rows} onContentChanged={this.props.onContentChanged} />
                </MDBModalBody>
                <MDBModalFooter>
                    <MDBBtn color="secondary" onClick={this.props.onCloseClick}>Close</MDBBtn>
                    <MDBBtn color="primary" disabled={this.props.shareDisabled} onClick={this.props.onShareClick}>
                        Share
                    </MDBBtn>
                </MDBModalFooter>
            </MDBModal>
        );
    };
}