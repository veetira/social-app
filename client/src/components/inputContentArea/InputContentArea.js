import React, {Component} from 'react'
import { MDBInput } from "mdbreact";

export default class InputContentArea extends Component{
    render () {
        return (
            <MDBInput id="content-post-input"
                type="textarea"
                label={this.props.label}
                rows={this.props.rows}
                onChange={this.props.onContentChanged}
            />
        );
    };
};