import React, {Component} from "react";
import {MDBCardHeader, MDBNav, MDBNavItem, MDBNavLink} from "mdbreact";
class ManageFriendsToolbar extends Component{
    constructor(props){
        super(props);
        this.state = {
            activeItem: "1"
        }
    }

    toggle = tab => () => {
        const me = this;
        if (me.state.activeItem !== tab) {
            me.setState({
                activeItem: tab
            });
        }
    };

    render() {
        return (
            <MDBCardHeader>
                <MDBNav header className="nav-justified">
                    <MDBNavItem >
                        <MDBNavLink className={this.state.activeItem==="1" ? "active" : "" } onClick={this.toggle("1")} to="#">
                            Friends
                        </MDBNavLink>
                    </MDBNavItem>
                    <MDBNavItem>
                        <MDBNavLink className={this.state.activeItem==="2" ? "active" : "" } onClick={this.toggle("2")} to="#">Find
                            friends</MDBNavLink>
                    </MDBNavItem>
                </MDBNav>
            </MDBCardHeader>
        )
    }

}
export default ManageFriendsToolbar;