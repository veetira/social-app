import React from 'react';
import {Button} from "react-bootstrap";
import "./LinkBtn.css";

export default (props) =>
    <Button className="link-btn" href={props.route}>
        {props.text}
    </Button>
