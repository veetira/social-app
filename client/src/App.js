import React from 'react';
import './App.css';
import './containers/auth/Auth.css';
import MainNavBar from "./components/mainNavBar/MainNavBar";
import Routes from "./Routes";
import {getCurrentFriends, getCurrentNotFriends, updateFriends, updateNotFriends} from "./containers/people/People";

export default class App extends React.Component{
    constructor () {
        super();
        this.state = {
            collapse: false,
            isWideEnough: false,
            notifications: []
        };
    }
    // getNotifications(){
    //     fetch("http://localhost:9000/users/get_friend_requests")
    //         .then(res => {
    //             return res.json();
    //         })
    //         .then(dataJson => {
    //             this.setState({
    //                 notifications: dataJson.data
    //             })
    //         })
    //         .catch(err => {
    //             alert(err)
    //         });
    // }
    // componentDidMount(){
    //     this.getNotifications();
    // }
    //
    // handleRequestAccept () {
    //     const me = this;
    //     return (ev) => {
    //         ev.preventDefault();
    //         const target = ev.target,
    //             requestSenderEmail = target.getAttribute("data-email");
    //         fetch("http://localhost:9000/users/accept_friend_request", {
    //                 method: "POST",
    //                 body: JSON.stringify({requestSenderEmail: requestSenderEmail}),
    //                 headers:{
    //                     'Content-Type': 'application/json'
    //                 }
    //             })
    //             .then(res => {
    //                 return res.json();
    //             })
    //             .then(dataJson => {
    //                 const notifications = me.state.notifications;
    //                 const friends = getCurrentFriends();
    //                 const notFriends = getCurrentNotFriends();
    //                 let newFriend;
    //                 for (let i = notifications.length - 1; i >= 0; i--) {
    //                     if (notifications[i].email === requestSenderEmail) {
    //                         notifications.splice(i, 1);
    //                     }
    //                 }
    //                 me.setState({
    //                     notifications: notifications
    //                 });
    //                 for (let i = notFriends.length - 1; i >= 0; i--) {
    //                     if (notFriends[i].email === requestSenderEmail) {
    //                         newFriend = notFriends[i];
    //                         console.log("New friend: ", JSON.stringify(newFriend))
    //                         notFriends.splice(i, 1);
    //                         break;
    //                     }
    //                 }
    //                 friends.push(newFriend);
    //                 updateFriends(friends);
    //                 updateNotFriends(notFriends);
    //             })
    //             .catch(err => {
    //                 //TODO replace with relevant functionality
    //                 alert(err)
    //             });
    //     }
    // }
    //
    // handleRequestDeny () {
    //     const me = this;
    //     return (ev) => {
    //         ev.preventDefault();
    //         const target = ev.target,
    //             requestSenderEmail = target.getAttribute("data-email");
    //         fetch("http://localhost:9000/users/deny_friend_request", {
    //             method: "POST",
    //             body: JSON.stringify({requestSenderEmail: requestSenderEmail}),
    //             headers:{
    //                 'Content-Type': 'application/json'
    //             }
    //         })
    //             .then(res => {
    //                 return res.json();
    //             })
    //             .then(dataJson => {
    //                 const notifications = me.state.notifications;
    //                 for (let i = notifications.length - 1; i >= 0; i--) {
    //                     if (notifications[i].email === requestSenderEmail) {
    //                         notifications.splice(i, 1);
    //                     }
    //                 }
    //                 me.setState({
    //                     notifications: notifications
    //                 });
    //             })
    //             .catch(err => {
    //                 //TODO replace with relevant functionality
    //                 alert(err)
    //             });
    //     }
    // }

    render () {
        const NavigationBar = () => {
            const route = window.location.pathname;
            return route === "/" || route === "/login" || route === "/registration" ? ""
                : <MainNavBar/>
        };

        return (
            <div className="App container">
                <NavigationBar/>
                <Routes />
            </div>
        );
    }

}

