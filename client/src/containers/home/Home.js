import React from 'react';
import { MDBCard, MDBCardBody, MDBRow, MDBCol, MDBListGroup } from 'mdbreact';
import Post from "../../components/post/Post";
import CreatePost from "../../components/post/CreatePost";
import {getUser} from "../../components/mainNavBar/MainNavBar";
import {isLoggedIn} from "../auth/Login";
import serverConfig from "../../config/server";

const baseUrl = serverConfig.protocol + "://" + serverConfig.host + ":" + serverConfig.port + "/posts";
export default class Home extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            posts: [],
            modal14: false,
            emptyContent: true
        };
    }

    getPostsData() {
        fetch(baseUrl)
            .then(res => {
                return res.json();

            })
            .then(dataJson => {
                this.setState({
                    posts: dataJson.data
                })
            })
            .catch(err => {
                alert(err)
            });
    }
    toggle = nr => () => {
        let modalNumber = 'modal' + nr;
        this.setState({
            [modalNumber]: !this.state[modalNumber]
        });
    };
    handleContentChange = ev => {
        this.setState({
            emptyContent: !(ev.target.value && ev.target.value.length)
        });
    };
    postMessage = () => {
        const contentPostInput = document.getElementById("content-post-input"),
            postContent = contentPostInput ? contentPostInput.value : null;
        if (postContent && postContent.length) {
            return fetch(baseUrl + "/create_post", {
                method: "POST",
                body: JSON.stringify({message: postContent}),
                headers:{
                    'Content-Type': 'application/json'
                }
            })
                .then(res => {
                    return res.json();

                })
                .then(dataJson => {
                    const postData = dataJson.data,
                        currentPosts = this.state.posts;
                    currentPosts.unshift({
                        name: getUser().name,
                        date: postData.date,
                        message: postData.message,
                        src: getUser().src
                    });
                    this.setState({
                        posts: currentPosts,
                        emptyContent: true,
                        modal14: !this.state.modal14
                    });
                })
                .catch(
                    error => console.error('Error:', error)
                );
        }
    };

    /**
     * Get data after the component rendered
     */
    componentDidMount() {
        this.getPostsData();
    }

    render() {
        /* Fill content to a single post*/
        const PostContent = ({ message: { name, src, date, message } }) => (
                <Post auth={name} avatarSrc={src} message={message} when={date}/>
            ),
            /* Bild all posts*/
            Posts =() => {
                return this.state.posts && this.state.posts.length ?
                    this.state.posts.map((post, index)  => (
                        <PostContent
                            key={index}
                            message={post}
                        />
                    )): "";
            };
        return (
            isLoggedIn ?
            <MDBCard className="grey lighten-3 chat-room">
                <MDBCardBody>
                    <MDBRow className="px-lg-2 px-2">
                        <MDBCol md="6" xl="8" className="pl-md-3 px-lg-auto mt-2 mt-md-0" style={{margin: "0 auto"}}>
                            <MDBRow>
                                <MDBListGroup className="list-unstyled pl-3" style={{marginTop: 50}}>
                                    <CreatePost userAvatar={getUser().src} isOpen={this.state.modal14} toggle={this.toggle(14)}
                                        shareDisabled={this.state.emptyContent} onShareClick={this.postMessage}
                                        onContentChanged={this.handleContentChange}
                                    />
                                    <Posts/>
                                </MDBListGroup>
                            </MDBRow>
                        </MDBCol>
                    </MDBRow>
                </MDBCardBody>
            </MDBCard> : ""
        );
    }
}