import React from "react";
import UsersPanel from "../../components/usersPanel/UsersPanel"
import CoolTabs from 'react-cool-tabs';
import serverConfig from "../../config/server";
import {isLoggedIn} from "../auth/Login";

const baseUrl = serverConfig.protocol + "://" + serverConfig.host + ":" + serverConfig.port + "/users";

export function updateFriends (friends) {
    this.setState({
        friends: friends
    });
}

export function updateNotFriends(notFriends) {
    this.setState({
        notFriends: notFriends
    });
}
export function getCurrentFriends() {
    return this.state.friends;
}
export function getCurrentNotFriends() {
    return this.state.notFriends;
}
export default class People extends React.Component{

    constructor (props) {
        super(props);
        this.state = {
            friends: [],
            notFriends: []
        };
        updateFriends = updateFriends.bind(this);
        updateNotFriends = updateNotFriends.bind(this);
        getCurrentFriends = getCurrentFriends.bind(this);
        getCurrentNotFriends = getCurrentNotFriends.bind(this);
    }

    getFriends() {
        fetch(baseUrl + "/friends")
            .then(res => {
                return res.json();

            })
            .then(dataJson => {
                this.setState({ friends: dataJson.data })
            })
            .catch(err => {
                alert(err)
            });
    }
    getNotFriends() {
        fetch(baseUrl + "/not_friends")
            .then(res => {
                return res.status === 200 ? res.json() : res.statusText;

            })
            .then(dataJson => {
                if (typeof dataJson === "object") {
                    const friendRequestsUsers = dataJson.friendRequestsUsers,
                        otherNotFriends = dataJson.otherNotFriends;
                    for (let i = 0; i < friendRequestsUsers.length; i++) {
                        friendRequestsUsers[i].requestSent = 1
                    }

                    this.setState({ notFriends: friendRequestsUsers.concat(otherNotFriends) });
                }
                else console.log("Error in retrieving not friends data: " + dataJson);
            });
    }
    componentDidMount() {
        this.getFriends();
        this.getNotFriends();
    }
    render () {
        return (isLoggedIn ?
            <CoolTabs className="users-panel"
                tabKey={'1'}
                style={{ maxWidth:  800, width: 100+"%", height: 1000, background:  'white', margin: "80px auto"}}
                activeTabStyle={{ background:  'lightgray', color:  'black' }}
                unActiveTabStyle={{ background:  'transparent', color:  'black' }}
                activeLeftTabBorderBottomStyle={{ background:  'transparent', height:  4 }}
                activeRightTabBorderBottomStyle={{ background:  'transparent', height:  4 }}
                tabsBorderBottomStyle={{ background:  'transparent', height:  4 }}
                leftContentStyle={{ background:  'transparent' }}
                rightContentStyle={{ background:  'transparent' }}
                leftTabTitle={'Friends'}
                rightTabTitle={'Find friends'}
                leftContent={<UsersPanel friendRequest="false" data={this.state.friends}/>}
                rightContent={<UsersPanel friendRequest="true" data={this.state.notFriends}/>}
                contentTransitionStyle={'transform 0.6s ease-in'}
                borderTransitionStyle={'all 0.6s ease-in'}/> :""
        );
    };

};