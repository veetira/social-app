import React, {Component} from "react";
import { MDBContainer, MDBRow, MDBCol, MDBBtn, MDBInput } from "mdbreact";
import serverConfig from "../../config/server";

const baseUrl = serverConfig.protocol + "://" + serverConfig.host + ":" + serverConfig.port + "/users";

export default class Registration extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
            name: "",
            email: "",
            city: "",
            password: "",
            confirmPassword: "",
            newUser: null
        }
    }

    handleChange = event => {
        this.setState({
            [event.target.id]: event.target.value
        });
    };

    validateForm() {
        return (
            this.state.name.length > 0 &&
            this.state.email.length > 0 &&
            this.state.city.length > 0 &&
            this.state.password.length > 0 &&
            this.state.password === this.state.confirmPassword
        );
    };

    handleSubmit = async event => {
        event.preventDefault();

        this.setState({ isLoading: true });
        fetch(baseUrl + "/signup", {
            method: "POST",
                body: JSON.stringify(
                    {
                        email: this.state.email,
                        password: this.state.password,
                        name: this.state.name,
                        city: this.state.city
                    }),
                headers:{
                'Content-Type': 'application/json'
            }
        }).then(res => {
            return res.json();
        }).then(response => {
            if (response.success) {
                this.props.history.push('/login');
            }
                else alert(response.error);
            }
        ).catch(
            error => {console.error('Error:', error)}
        );
    };

    render() {
        return (
            <MDBContainer>
                <MDBRow>
                    <MDBCol md="6" style={{margin: '50px auto', maxWidth: 400}}>
                        <form onSubmit={this.handleSubmit}>
                            <p className="h5 text-center mb-4">Sign up</p>
                            <div className="grey-text">
                                <MDBInput id="name"
                                    label="Your name"
                                    icon="user"
                                    group
                                    type="text"
                                    validate
                                    error="wrong"
                                    success="right"
                                    onChange={this.handleChange}
                                />
                                <MDBInput id="email"
                                    label="Type your email"
                                    icon="envelope"
                                    group
                                    type="email"
                                    validate
                                    error="wrong"
                                    success="right"
                                    onChange={this.handleChange}
                                />
                                <MDBInput id="city"
                                    label="Your city"
                                    icon="city"
                                    group
                                    type="text"
                                    validate
                                    error="wrong"
                                    success="right"
                                    onChange={this.handleChange}
                                />
                                <MDBInput id="password"
                                    label="Your password"
                                    icon="lock"
                                    group
                                    type="password"
                                    validate
                                    onChange={this.handleChange}
                                />
                                <MDBInput id="confirmPassword"
                                    label="Confirm your password"
                                    icon="exclamation-triangle"
                                    group
                                    type="password"
                                    validate
                                    onChange={this.handleChange}
                                />
                            </div>
                            <div className="text-center">
                                <MDBBtn color="primary" className="btn-block z-depth-2" disabled={!this.validateForm()} type="submit">Create an account</MDBBtn>
                            </div>
                            <p className="font-small grey-text d-flex justify-content-center" style={{marginTop: 20}}>
                                Already registered?
                                <a
                                    href="/login"
                                    className="dark-grey-text font-weight-bold ml-1"
                                >
                                    Sign in
                                </a>
                            </p>
                        </form>
                    </MDBCol>
                </MDBRow>
            </MDBContainer>
        );
    };
}