import React, {Component} from "react";
import { MDBContainer, MDBRow, MDBCol, MDBInput, MDBBtn } from 'mdbreact';
import serverConfig from "../../config/server";

const baseUrl = serverConfig.protocol + "://" + serverConfig.host + ":" + serverConfig.port + "/users";

export function isLoggedIn() {
    return this.state.loggedIn;
}
export default class Login extends Component {
    constructor(props) {
        super(props);

        this.state = {
            email: '',
            password: '',
            loggedIn: false
        };
        isLoggedIn = isLoggedIn.bind(this);
    }

    validateForm() {
        return this.state.email.length > 0 && this.state.password.length > 0;
    }

    handleChange = event => {
        this.setState({
            [event.target.id]: event.target.value
        });
    };

    handleSubmit = event => {
        event.preventDefault();
        return fetch(baseUrl + "/login", {
            method: "POST",
            body: JSON.stringify({email: this.state.email, password: this.state.password}),
            headers:{
                'Content-Type': 'application/json'
            }
        }).then(res => {
            return res.json();
        }).then(response => {
            if (response.success) {
                this.setState({loggedIn: true});
                this.props.history.push('/home');
            }
            else {
                this.setState({loggedIn: false});
                alert(response.error)
            }
        }).catch(
            error => {this.setState({loggedIn: false}); console.error('Error:', error)}
        );
    };
    render() {
        return (
            <MDBContainer>
                <MDBRow>
                    <MDBCol md="6" style={{margin: '50px auto', maxWidth: 400}}>
                        <form onSubmit={this.handleSubmit}>
                            <p className="h5 text-center mb-4">Sign in</p>
                            <div className="grey-text">
                                <MDBInput id="email"
                                    label="Type your email"
                                    icon="envelope"
                                    group
                                    type="email"
                                    validate
                                    error="wrong"
                                    success="right"
                                          onChange={this.handleChange}
                                />
                                <MDBInput id="password"
                                    label="Type your password"
                                    icon="lock"
                                    group
                                    type="password"
                                    validate
                                          onChange={this.handleChange}
                                />
                            </div>
                            <div className="text-center">
                                <MDBBtn
                                    className="btn-block z-depth-2"
                                    disabled={!this.validateForm()}
                                    type="submit">Login</MDBBtn>
                            </div>
                            <p className="font-small grey-text d-flex justify-content-center" style={{marginTop: 20}}>
                                Don't have an account?
                                <a
                                    href="/registration"
                                    className="dark-grey-text font-weight-bold ml-1"
                                >
                                    Sign up
                                </a>
                            </p>
                        </form>
                    </MDBCol>
                </MDBRow>
            </MDBContainer>
        );
    }
};